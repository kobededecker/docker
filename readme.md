***
> **Note**: if your are working on _Windows 7 || Windows 10_ you have to put your code
> under  `C:\\users\yourusername\` otherwise _Docker_ **will NOT** mount your volumes.

***

# Setting up a Dockerized Development Environment

Clone this repo by doing `git clone http://gitlab.com/kobededecker/docker`

Move between steps by doing `git checkout step-1`, `git checkout step-2` ...

## 0. Some docker concepts:

Docker is used for bundling software with an OS, so that it can be used and deployed anywhere without installation of dependencies. However, the linux kernel is shared with the host.

### Image vs container

* Docker image: An immutable bundle, consisting of cached layers, each representing filesystem changes.
* Docker Container: an active or running instance of an image, with a set of run-time changes. Multiple containers can be created from one image.

### Dockerfile vs docker-compose.yml

* Dockerfile: defines the Build parameters of (one) Docker image. Each line is cached as a layer.
* Docker-compose.yml: defines the Run-time parameters of (one or multiple) Docker containers, the images to use and the relations between different containers.

### Docker-machine

* Tool for managing different docker hosts: we have only one: the local Docker host.
* Not native on Windows 7 (native on future versions of windows 10). The Docker host lives in a VirtualBox
* Needs to be started by typing: `docker-machine start default` or `docker-machine start`

### Docker environment

* When opening a new shell, it needs to be targeted towards a specific Docker machine
* `docker-machine env default` results in a set of environment variables
* On Windows Shell: `for /f "tokens=*" %i in ('docker-machine env') do %i `
* in this GIT project: run `setupDocker.bat`



